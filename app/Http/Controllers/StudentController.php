<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function index(){
        $studentlist = student::paginate(2);
        return response()->json($studentlist,200);
    }

    function search($name){
        
        return Student::where("name","like","%".$name."%")->get();
    
    }
}
