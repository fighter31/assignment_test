<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = "student";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
        'phone_number',
        'email',
        'country',
        'coutry_code',

    ];
}
